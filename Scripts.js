//Файл Script.js
//Фігури у 1-0 формі
//Елемент Figures - це тип фігури , 0 елемент,наприклад, фігурка палка
//Записи до ":" - це положення при обертанні - всього їх може бути 4
//Записи через "|" - рівні фігури відносно осі y

var Figures = [
        "1111:01|01|01|01:1111:01|01|01|01",
        "011|110:010|011|001:011|110:010|011|001",
        "110|011:001|011|010:110|011:001|011|010",
        "111|010:01|11|01:010|111:10|11|10",
        "11|11:11|11:11|11:11|11",
        "010|010|011:111|100:11|01|01:001|111",
        "01|01|11:100|111:11|10|10:111|001",
    //"100|100|011:011|100|100:110|001|001:001|001|110"//можливість додавання фігури 4*4
];
var    now ;
var    pos = [4, -1];

var Score_barrier = [100 , 250,500,1000,1500,2000,2500,3250,4000,4750,5500,6250];

var Level=0;
var Future_Fig;
const Variants_Rotation = 4;
const Width_Count = 10;
const Heigth_Count = 20;
const Speed_Up = 50;
const Tick_Tac_Proto = 600;
const Code_Key_Pause = 32;
let tick_tac = Tick_Tac_Proto;
let Is_Enabled = true;


let Figures_Bool = [];

//Чітко сформований блок завантаження скрипту
function Load_This_Script() {

    Cteating_Figures_Bool();
    now = [Random_Index_Fig(), 0];
    Passage_Field_Future();
    Set_Tick_Tac_Relative_Level();

//Таймер з динамічним кроком tick_tac
    setTimeout(function tick() {
        Processing_Keys({
            keyCode: 40
        });
        /*Random_Test();*/
        setTimeout( tick,tick_tac);
    }, tick_tac);
}

Load_This_Script();


//Автоматична зміна кроку таймера , якщо рівень задається не з Нульового
function Set_Tick_Tac_Relative_Level() {
    tick_tac = Tick_Tac_Proto - Level*Speed_Up;
    document.querySelector('#Level').innerHTML = Level;
}



//Відповідає за підвищення рівня , якщо набрана відповідна кількість очків
function Level_Updating() {
    let Active_Score = Math.floor(document.querySelector('#Score').innerHTML);
    if(Level<Score_barrier.length && Active_Score>Score_barrier[Level])
    {
        ++Level;
        tick_tac-=Speed_Up;
        document.querySelector('#Level').innerHTML = Level;

    }
}

/*var mass = [0,0,0,0,0,0,0];*/
//Адаптація однорівневого масиву Figures у чотирьохрівневий Figures_Bool bool-орієнтований для пришвидшення роботи
function Cteating_Figures_Bool() {

    for (let x=0;x<Figures.length; ++x) {
        Figures_Bool[x] = [];
        let Type_Fig = Figures[x].split(':');
        for (let y=0;y<Type_Fig.length; ++y) {
            Figures_Bool[x][y] = [];

            let Turn = Type_Fig[y].split('|');

            for (let i=0;i<Turn.length; ++i) {
                Figures_Bool[x][y][i] = [];

                let Level = Turn[i].split('');

                for (let j=0;j<Level.length; ++j) {
                    Figures_Bool[x][y][i][j] = ((Level[j] == "1") ? true : false);


                }
            }
        }
    }
    return true;
}

//Створення заповнення рядку крім першого(для тестування)
function Test_Fulling_Row(y) {
    for (let x=1;x<Width_Count; ++x) {
        gP(x, y).classList.add('on');
    }
}
//Робота з Селектором відповідно до заданих координат
function gP(x, y) {
    return document.querySelector('[data-y="' + y + '"] [data-x="' + x + '"]');
}
//Перемалювання Активної Фігури
function Draw(ch, cls) {
    
    if( Passage_Active_Field(ch ,undefined, true) ) {
        if(cls=='on') {
            
            Passage_Active_Field([0,0] ,'on' , false);
            
        }
        return false;
    }

    Passage_Active_Field(ch ,undefined, false);

    pos = [pos[0] + ch[0], pos[1] + ch[1]];
}
//Працює у двох режимах Mode :
// При true визначає чи можливе переміщення , а
// При false виконується перемалювання активної фігури
function Passage_Active_Field(ch ,cls, Mode) {

    let f = Figures_Bool[now[0]][now[1]];
    /*Check_Row(roll(i), document.querySelector('#result').innerHTML= f;*/
    let X_Delta = pos[0] + ch[0];
    let Y_Delta = pos[1] + ch[1];

    if(Mode==false){
        Cleaning_Brick('now');
    }

    for (let y = 0; y < f.length; y++) {

        for (let x = 0; x < f[y].length; x++) {

            if (f[y][x] == true) {

                let _X_ = x + X_Delta;
                let _Y_ = y + Y_Delta;

                if(Mode==true) {
                    if (Width_Count <= _X_ || _X_ < 0 || _Y_ >= Heigth_Count || gP(_X_, _Y_).classList.contains('on')) {
                        return true;
                    }
                }
                else{
                    gP(_X_, _Y_).classList.add(cls !== undefined ? cls : 'now');
                }

            }

        }
    }
}
//Відображає наступну фігуру
function Passage_Field_Future() {

    Future_Fig = Random_Index_Fig();
    let f = Figures_Bool[Future_Fig][0];

        Cleaning_Brick('Future');

    for (let y = 0; y < f.length; ++y) {
        for (let x = 0; x < f[y].length; ++x) {

            if (f[y][x] == true) {
                    gP(x, y-4).classList.add('Future');

            }
        }
    }
}
//Очищує заданий css-стиль
function Cleaning_Brick(Title) {
    let BrickNOW = document.querySelectorAll('.'+Title);

    for(let i=0; BrickNOW.length>i ;++i)
    {
        BrickNOW[i].classList.remove(Title);
    }
}

//Пошук , знищення Заповнених рядків і додавання за це балів
function Check_Row() {

    const Score_Zero = 0;
    const Score_Scale = 10;
    let Row_Full = [];
    let Score = Score_Zero;



    for (let i = 0; i < Heigth_Count; ++i) {
        if (document.querySelectorAll('[data-y="' + i + '"] .brick.on').length == Width_Count) {
            Row_Full[i] = true;
            ++Score;
        }
        else {
            Row_Full[i] = false;
        }
    }

    if(Score==Score_Zero)
    {
        return false;
    }


    let Active_Row = Heigth_Count;
    for (let y = Heigth_Count-1; y >=0; --y) {
        if (Row_Full[y]==false) {
            --Active_Row;
            if(y!=Active_Row)
            {
                roll(y,Active_Row);

            }

        }
    }

    if(Score!=Score_Zero)
    {
        let Add_Score = ((1<<Score)-1)*Score_Scale;
        document.querySelector('#Score').innerHTML = Math.floor(document.querySelector('#Score').innerHTML)+Add_Score;
        Level_Updating();
    }
}

//Зсунення рядка стилів у заданий рядок
function roll(y,Active_Row) {
Selector_Row(Active_Row).innerHTML = Selector_Row(y).innerHTML;
}
//Повернення заданого рядка стилів
function Selector_Row(y)
{
    return document.querySelector('[data-y="' + y + '"]');
}
//Додавання події натиснення Кдавіші
window.addEventListener('keydown',EventListener = function(e){

    let Percent_Top = Math.floor((pos[1]/Heigth_Count)*100);

    if(e.keyCode==40 && (Percent_Top<8))
    {
        return false;
    }
    Processing_Keys(e);


} );

//Обробка натиснення клавіші
function Processing_Keys(e) {
    /*let prv;*/
    if(!Is_Enabled  &&  e.keyCode!=Code_Key_Pause)
    {
        return false;
    }

    switch (e.keyCode) {
        case 38:

            let prv = now[1];
            now[1] = (++now[1]) % Variants_Rotation;

            if ( false === Draw([0, 0], undefined) ) {
                now = [now[0], prv];
            }

            break;


        case 39:
        case 37:


            Draw([e.keyCode == 39 ? 1 : -1, 0], undefined ) ;

            break;



        case 40:


            let Condition = false === Draw([0, 1], 'on');
            if (Condition) {
                Check_Row();

                now = [Future_Fig, 0];
                pos = [4, 0];
                Passage_Field_Future();

                if (false === Draw([0, 0], undefined)) {
                    Game_Over();
                    Cleaning_Brick('on');
                    Cleaning_Brick('now');
					document.querySelector('#Score').innerHTML = 0;
					Level=0;

                    document.querySelector('#Level').innerHTML = Level;
					
					Set_Tick_Tac_Relative_Level();
					pos = [4, -1];
                        Is_Enabled = true;
                }
            }
            break;
        case Code_Key_Pause:
            Is_Enabled = !Is_Enabled;
    }


}
//Введення результату гри та відправлення даних на сервер
function  Game_Over() {
    Is_Enabled = false;
    alert('Your score: ' + document.querySelector('#Score').innerHTML);
}



//Генерування індекса наступної Фігури
function Random_Index_Fig() {
    return Math.floor(Math.random() * Figures.length);
}

